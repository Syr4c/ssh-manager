### SSH Manager 0.1 ###

*Chris Thiele, chris.thiele@haw-hamburg.de, 03.01.2016*


Ein kleines Tool zum speichern und verwalten von SSH Server Adressen und Loginnamen.

Damit der SSH Manager die richtigen Loginnamen und Server erkennt, ist es notwendig,
das im Root Ordner des Scripts ein Unterordner mit dem Namen "ssh_conf" angelegt wird.
In diesen koennen dann beliebig viele Datein mit der Endung *.ssh_conf gespeichert werden.

Die *.ssh_conf Dateien muessen zwangslaeufig folgende Struktur aufweisen:

* 1.Login Name
* 2.Login Name
* 3.Login Name
* ...
* n.ServerAdresse

Die Server Adresse muss an letzter Stelle stehen, da sie sonst nicht korrekt aus der Datei
geparst werden kann.


Script wurde getestet unter Manjaro 15.12 Capella @ i686 Linux 4.1.15-1-MANJARO.