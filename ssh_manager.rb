# A simple SSH-Connection Manager
# v0.1 Chris Thiele 2016
# chris.thiele@haw-hamburg.de
# 

class Ssh
	def initialize
		@all_conf_files = Dir.glob "ssh_conf/*.ssh_conf"
		@server_objects = Array.new
		@arr_arr = Array.new
		@server
		@server_number
		@name
		data_input
	end

	def data_input
		@all_conf_files.each do |file_name|
			@server_objects.push File.open(file_name)
		end

		@server_objects.each do |file_object|
			temp_array = file_object.read.split(/\r?\n/)
			@arr_arr.push temp_array
		end
		welcome
	end

	def welcome
		print("A simple SSH-Connection Manager\n\n")
		print("Version 0.1, Chris Thiele 2016\n")
		print("chris.thiele@haw-hamburg.de\n")
		print("================================\n\n")
		print("Please choose a server: \n")
	end

	def choose_server
		@arr_arr.each_with_index do |array_obj, index|
			print("#{index+1}. #{array_obj.last}\n")
		end
		begin
			print("\n>")
			user_eingabe = gets.chomp
			user_eingabe.gsub!(/[^0-9]/, '')

			if user_eingabe.to_i <= 0 || user_eingabe.to_i > @arr_arr.length+1
				raise ArgumentError
			else
				@server_number = user_eingabe.to_i-1
				@server = @arr_arr[user_eingabe.to_i-1].pop
				choose_name
			end
		rescue ArgumentError
			print("\nServer can't be choosen, please try again.\n")
			choose_server
		end
	end

	def choose_name
		print("\nPlease choose a login name:\n")
		@arr_arr[@server_number].each_with_index do |name, index|
			print("#{index+1}. #{name}\n")
		end
		begin
			print("\n>")
			user_eingabe = gets.chomp
			user_eingabe.gsub!(/[^0-9]/, '')

			if user_eingabe.to_i <= 0 || user_eingabe.to_i > @arr_arr[@server_number].length+1
				raise ArgumentError
			else
				@name = @arr_arr[@server_number][user_eingabe.to_i-1]
				establish_connection
			end
		rescue ArgumentError
			print("\nName can't be choosen, please try again.\n")
			choose_name
		end
	end

	def establish_connection
		print("Connecting to: #{@server} as #{@name}\n\n")
		exec( "ssh #{@name}@#{@server}")
	end
end

Ssh.new.choose_server